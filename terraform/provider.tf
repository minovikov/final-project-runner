terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  service_account_key_file = "key.json"
  cloud_id                 = "b1g8jqnkoctmskpeboj2"
  folder_id                = "b1gjcuf6qq5euca3gutb"
  zone = "ru-central1-a"
}