data "yandex_compute_image" "my-ubuntu-1804-1" {
  family = "ubuntu-1804-lts"
}

resource "yandex_compute_instance" "vm-gl-runner-1" {
  name        = "vm-gl-runner-1"
  platform_id = "standard-v1"
  zone        = "ru-central1-a"

  resources {
    cores  = 2
    memory = 2
  }

  boot_disk {
    initialize_params {
      image_id = "${data.yandex_compute_image.my-ubuntu-1804-1.id}"
      size = 10
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.sn-gl-runner-1.id
    nat = true
  }

  metadata = {
    ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
  }
}
