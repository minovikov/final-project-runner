## IaC for gitlab-runner

### terraform
Для создания ВМ, на которой в последствии будет установлен и зарегистрирован gitlab-runner, необходимо:

1. Создать файл `provider.tf` с параметрами вашей УЗ в сервисе Yandex Cloud:

```
terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token     = ""
  cloud_id  = ""
  folder_id = ""
  zone      = ""
}
```

Для создания инфраструктуры под gitlab-runner:
```bash
terraform apply
```

### ansible
Перед запуском настройки ВМ и установки gitlab-runner необходимо:

1. Отредактировать файл `absible/hosts`, указав IP-адрес ВМ, развернутой с помощью terraform, а также путь до приватного ключа на вашем ПК.


#### Установка бинарника gitlab-runner
```bash
ansible-playbook gl-runner.yml -t install_runner
```

#### Регистрация gitlab-runner
```bash
ansible-playbook gl-runner.yml -t add_runner
```

#### Удаление gitlab-runner
```bash
ansible-playbook gl-runner.yml -t del_runner
```